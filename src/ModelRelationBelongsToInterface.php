<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelRelationInterface interface file.
 * 
 * This interface represents a belongs to relation between two model objects.
 * This means that the source object belongs to the target object, which, in
 * relational databases, results in the value of an identifier to be copied
 * from the target object to the source object with a constraint on the source
 * object that the keys that represents the target exists in the target table.
 * 
 * @author Anastaszor
 */
interface ModelRelationBelongsToInterface extends ModelRelationInterface
{
	
	// nothing to add
	
}
