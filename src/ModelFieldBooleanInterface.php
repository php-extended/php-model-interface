<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelFieldBooleanInterface interface file.
 * 
 * This interface represents a boolean field.
 * 
 * @author Anastaszor
 */
interface ModelFieldBooleanInterface extends ModelFieldInterface
{
	
	/**
	 * Gets the default boolean value for this field.
	 * 
	 * @return boolean
	 * @SuppressWarnings("PHPMD.BooleanGetMethodName")
	 */
	public function getDefaultValue() : bool;
	
}
