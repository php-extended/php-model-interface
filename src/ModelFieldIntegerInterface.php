<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelFieldIntegerInterface interface file.
 * 
 * This interface represents an integer field.
 * 
 * @author Anastaszor
 */
interface ModelFieldIntegerInterface extends ModelFieldInterface
{
	
	/**
	 * Gets the capacity of the integer, in bits. An integer with 32 bits
	 * capacity should range from 0 to 2^32-1 (unsigned) or from -2^31 to
	 * 2^31-1 (signed).
	 * 
	 * @return IntegerCapacityInterface
	 */
	public function getCapacity() : IntegerCapacityInterface;
	
	/**
	 * Whether this field contains a signed integer. This returns true if the
	 * integer should be signed, false otherwise.
	 * 
	 * @return boolean
	 */
	public function isSigned() : bool;
	
	/**
	 * Gets the default integer value for this field.
	 * 
	 * @return ?integer
	 */
	public function getDefaultValue() : ?int;
	
}
