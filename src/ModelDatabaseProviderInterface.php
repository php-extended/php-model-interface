<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use Stringable;

/**
 * ModelDatabaseProviderInterface interface file.
 * 
 * This interface represents a provider of model databases. This provider is
 * provided to populate databases to a RDBMS.
 * 
 * @author Anastaszor
 */
interface ModelDatabaseProviderInterface extends Stringable
{
	
	/**
	 * Lists all the model databases that are needed to describe databases.
	 * 
	 * @return array<integer, ModelDatabaseInterface>
	 */
	public function listModelDatabases() : array;
	
}
