<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Optionality\OptionalityInterface;
use Stringable;

/**
 * ModelFieldInterface interface file.
 * 
 * This interface represents an object to modelize a field. A field represents
 * a named space to store data in a model object.
 * 
 * @author Anastaszor
 */
interface ModelFieldInterface extends Stringable
{
	
	/**
	 * The name of the field.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the optionality of this field.
	 * 
	 * The optionality of this interface describes whether this field allows
	 * null or undefined, or empty values (for certain values of empty).
	 * 
	 * @return OptionalityInterface
	 */
	public function getOptionality() : OptionalityInterface;
	
	/**
	 * The internationalizable status of the field, meaning whether this field
	 * has its place in the data model, or its internationalized variant, if
	 * the choice is available.
	 * 
	 * @return InternationalizableStatusInterface
	 */
	public function getInternationalizedStatus() : InternationalizableStatusInterface;
	
	/**
	 * Gets an associated comment to this field.
	 * 
	 * @return ?string
	 */
	public function getComment() : ?string;
	
	/**
	 * Merges this field with another given field into a new field object.
	 * 
	 * @param ModelFieldInterface $field
	 * @return ModelFieldInterface
	 */
	public function mergeWith(ModelFieldInterface $field) : ModelFieldInterface;
	
	/**
	 * Visits this model field with the given visitor.
	 * 
	 * @param ModelFieldVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelFieldVisitorInterface $visitor);
	
}
