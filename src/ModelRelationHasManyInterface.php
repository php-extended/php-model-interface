<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelRelationHasManyInterface interface file.
 * 
 * This interface represents an has many relation between two model objects.
 * This means that the target object belongs to the source object, which, in
 * relational databases, results in the value of an identifier to be copied
 * from the source object to the target object with a constraint on the target
 * object that the keys that represents the source exists in the source table.
 * 
 * @author Anastaszor
 */
interface ModelRelationHasManyInterface extends ModelRelationInterface
{
	
	// nothing to add
	
}
