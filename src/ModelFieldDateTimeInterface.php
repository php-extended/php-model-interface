<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use DateTimeInterface;
use PhpExtended\DateTimeFormat\DateTimeFormatInterface;

/**
 * ModelFieldDateTimeInterface interface file.
 * 
 * This interface represents a date and/or time field.
 * 
 * @author Anastaszor
 */
interface ModelFieldDateTimeInterface extends ModelFieldInterface
{
	
	/**
	 * Gets the accepted format of this date and/or time field.
	 * 
	 * @return DateTimeFormatInterface
	 */
	public function getFormat() : DateTimeFormatInterface;
	
	/**
	 * Gets whether this field should be initialized with current timestamp on
	 * create.
	 * 
	 * @return boolean
	 */
	public function isInitializedOnCreate() : bool;
	
	/**
	 * Gets whether this field should be modified with current timestamp on
	 * update.
	 * 
	 * @return boolean
	 */
	public function isModifiedOnUpdate() : bool;
	
	/**
	 * Gets the default value for this datetime field.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDefaultValue() : ?DateTimeInterface;
	
}
