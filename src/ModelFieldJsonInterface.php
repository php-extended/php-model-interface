<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelFieldJsonInterface interface file.
 * 
 * This interface represents a json object field.
 * 
 * @author Anastaszor
 */
interface ModelFieldJsonInterface extends ModelFieldInterface
{
	
	/**
	 * Gets the maximum depths (in number of nested objects or arrays) for 
	 * this json document.
	 * 
	 * @return integer
	 */
	public function getMaximumDepths() : int;
	
	/**
	 * Gets the default json value for this field.
	 * 
	 * @return ?string
	 */
	public function getDefaultValue() : ?string;
	
}
