<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelRelationManyManyInterface interface file.
 * 
 * This interface represents a many many relation between two model objects.
 * This means that the source object and the target object each belongs to a
 * shared object, which, in relational databases, results in the value of an
 * identifier from the source object and an identifier from the target object
 * to be both copied on a third object, with both constraints from the third 
 * object on the source object and on the target object that their respective
 * keys that represents the objects exists in their respective tables.
 * 
 * @author Anastaszor
 */
interface ModelRelationManyManyInterface extends ModelRelationInterface
{
	
	/**
	 * Gets the table name of the relation table formed with the given other
	 * data object.
	 * 
	 * @return string
	 */
	public function getRelationalTableName() : string;
	
	/**
	 * Gets the internationalized table name of the relation table formed with
	 * the given other data object.
	 * 
	 * @return string
	 */
	public function getI18nRelationalTableName() : string;
	
	/**
	 * Gets the table comment of the relation table formed with the given other
	 * data object.
	 * 
	 * @return ?string
	 */
	public function getRelationalTableComment() : ?string;
	
	/**
	 * Gets the internationalized table comment of the relation table formed
	 * with the given other data object.
	 * 
	 * @return ?string
	 */
	public function getI18nRelationalTableComment() : ?string;
	
}
