<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use Stringable;

/**
 * ModelObjectInterface interface file.
 * 
 * This interface represents an object to modelize a class. This model object
 * is populated from a table from a relational database.
 * 
 * @author Anastaszor
 * @todo reify FieldList
 * @todo reify IndexList
 * @todo reify RelationList
 */
interface ModelObjectInterface extends Stringable
{
	
	/**
	 * Gets whether this data object is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabled() : bool;
	
	/**
	 * Gets whether this data object is internationalized.
	 * 
	 * @return boolean
	 */
	public function isInternationalized() : bool;
	
	/**
	 * Gets the comment on this object.
	 * 
	 * @return null|string
	 */
	public function getComment();
	
	/**
	 * Gets the table name of the record for this metadata.
	 * 
	 * @return string
	 */
	public function getTableName() : string;
	
	/**
	 * Gets the internationalized table name for the record of this metadata.
	 * 
	 * @return string
	 */
	public function getI18nTableName() : string;
	
	/**
	 * Gets the primary key of the record for this metadata.
	 * 
	 * @return array<integer, string>
	 */
	public function getTablePk() : array;
	
	/**
	 * Gets the internationalized primary key for the record of this metadata.
	 * 
	 * @return array<integer, string>
	 */
	public function getI18nTablePk() : array;
	
	/**
	 * Gets the comment of the table for this metadata.
	 * 
	 * @return ?string
	 */
	public function getTableComment() : ?string;
	
	/**
	 * Gets the internationalized comment of the table for this metadata.
	 * 
	 * @return ?string
	 */
	public function getI18nTableComment() : ?string;
	
	/**
	 * Gets the fields this data object may have.
	 * 
	 * @return array<integer, ModelFieldInterface>
	 */
	public function getFields() : array;
	
	/**
	 * Gets the names of the indexes applied to this model.
	 * 
	 * @return array<integer, ModelObjectIndexInterface>
	 */
	public function getIndexes() : array;
	
	/**
	 * Gets the relations from this object to other data objects this object
	 * may have.
	 * 
	 * @return array<integer, ModelRelationInterface>
	 */
	public function getRelations() : array;
	
	/**
	 * Gets the relations that points to this objects. Those relations are
	 * the relations which reprensents the inverse relations of those get by
	 * the getRelations() method.
	 * 
	 * @return array<integer, ModelRelationInterface>
	 */
	public function getReverseRelations() : array;
	
	/**
	 * Merges this object and the given object into a new model object.
	 * 
	 * @param ModelObjectInterface $object
	 * @return ModelObjectInterface
	 */
	public function mergeWith(ModelObjectInterface $object) : ModelObjectInterface;
	
}
