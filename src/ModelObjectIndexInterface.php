<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use Stringable;

/**
 * ModelObjectIndexInterface interface file.
 *
 * This interface represents an index on a model object.
 *
 * @author Anastaszor
 */
interface ModelObjectIndexInterface extends Stringable
{
	
	/**
	 * Gets the name of the index.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the name of the fields that are included in the index. The order of
	 * the fields determines the indexing order.
	 * 
	 * @return array<integer, string>
	 */
	public function getFields() : array;
	
}
