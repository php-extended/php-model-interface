<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use Stringable;

/**
 * ModelRelationVisitorInterface interface file.
 * 
 * This interface represents a dispatcher to visit the different types of
 * relations.
 * 
 * @author Anastaszor
 */
interface ModelRelationVisitorInterface extends Stringable
{
	
	/**
	 * Visits a belongs-to relation.
	 * 
	 * @param ModelRelationBelongsToInterface $relation
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitBelongsToRelation(ModelRelationBelongsToInterface $relation);
	
	/**
	 * Visits a has-many relation.
	 * 
	 * @param ModelRelationHasManyInterface $relation
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitHasManyRelation(ModelRelationHasManyInterface $relation);
	
	/**
	 * Visits a many-many relation.
	 * 
	 * @param ModelRelationManyManyInterface $relation
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitManyManyRelation(ModelRelationManyManyInterface $relation);
	
}
