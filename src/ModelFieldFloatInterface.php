<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelFieldFloatInterface interface file.
 * 
 * This interface represents a float field.
 * 
 * @author Anastaszor
 */
interface ModelFieldFloatInterface extends ModelFieldInterface
{
	
	/**
	 * Gets the capacity of the float, in bits. The capacity bits contains the
	 * bits as well for the mantissa and for the exponent part of the float.
	 * 
	 * @return integer
	 */
	public function getCapacity() : int;
	
	/**
	 * Gets the exponent quantity of bits in the exponent part.
	 * 
	 * @return integer
	 */
	public function getExponent() : int;
	
	/**
	 * Gets the default value for this float field.
	 * 
	 * @return ?float
	 */
	public function getDefaultValue() : ?float;
	
}
