<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use Stringable;

/**
 * ModelObjectProviderInterface interface file.
 * 
 * This interface represents a provider of model objects. This provider is
 * provided to populate tables to a relational database.
 * 
 * @author Anastaszor
 */
interface ModelObjectProviderInterface extends Stringable
{
	
	/**
	 * Lists all the model objects that are needed to describe the database.
	 * 
	 * @return array<integer, ModelObjectInterface>
	 */
	public function listModelObjects() : array;
	
}
