<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Charset\CharacterSetInterface;

/**
 * ModelFieldStringInterface interface file.
 * 
 * This interface represents a string field.
 * 
 * @author Anastaszor
 */
interface ModelFieldStringInterface extends ModelFieldInterface
{
	
	/**
	 * Gets the minimum length (in bytes) of the string.
	 * 
	 * @return integer
	 */
	public function getMinimumLength() : int;
	
	/**
	 * Gets the maximum length (in bytes) of the string.
	 * 
	 * @return integer
	 */
	public function getMaximumLength() : int;
	
	/**
	 * Gets the character set of the string.
	 * 
	 * @return CharacterSetInterface
	 */
	public function getCharset() : CharacterSetInterface;
	
	/**
	 * Gets the default string value for this field.
	 * 
	 * @return ?string
	 */
	public function getDefaultValue() : ?string;
	
}
