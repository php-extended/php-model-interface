<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelFieldSpatialInterface interface file.
 * 
 * This interface represents a spatial field.
 * 
 * @author Anastaszor
 */
interface ModelFieldSpatialInterface extends ModelFieldInterface
{
	
	/**
	 * Gets the default json value for this field.
	 * 
	 * @return ?string
	 */
	public function getDefaultValue() : ?string;
	
}
