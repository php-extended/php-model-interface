<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelDatabaseInterface interface file.
 * 
 * This interface represents a database to modelize an ensemble of objects.
 * This model database is populated from a relational database.
 * 
 * @author Anastaszor
 */
interface ModelDatabaseInterface extends ModelSchemaProviderInterface
{
	
	/**
	 * Gets whether this database is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabled() : bool;
	
	/**
	 * Gets the name of the database in the RDBMS.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Merges this database and the given database into a new database object.
	 * 
	 * @param ModelDatabaseInterface $database
	 * @return ModelDatabaseInterface
	 */
	public function mergeWith(ModelDatabaseInterface $database) : ModelDatabaseInterface;
	
}
