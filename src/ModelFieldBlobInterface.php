<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelFieldBlobInterface interface file.
 * 
 * This interface represents a binary object field.
 * 
 * @author Anastaszor
 */
interface ModelFieldBlobInterface extends ModelFieldInterface
{
	
	/**
	 * Gets the minimum length (in bytes) of the binary object.
	 * 
	 * @return integer
	 */
	public function getMinimumLength() : int;
	
	/**
	 * Gets the maximum length (in bytes) of the binary object.
	 * 
	 * @return integer
	 */
	public function getMaximumLength() : int;
	
	/**
	 * Gets the default blob value for this field.
	 * 
	 * @return ?string
	 */
	public function getDefaultValue() : ?string;
	
}
