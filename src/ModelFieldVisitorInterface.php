<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use Stringable;

/**
 * ModelFieldVisitorInterface interface file.
 * 
 * This interface represents a dispatcher to visit the different types of
 * model fields.
 * 
 * @author Anastaszor
 */
interface ModelFieldVisitorInterface extends Stringable
{
	
	/**
	 * Visits a blob field.
	 * 
	 * @param ModelFieldBlobInterface $blobField
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitBlobField(ModelFieldBlobInterface $blobField);
	
	/**
	 * Visits a boolean field.
	 * 
	 * @param ModelFieldBooleanInterface $booleanField
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitBooleanField(ModelFieldBooleanInterface $booleanField);
	
	/**
	 * Visits a date and/or time field.
	 * 
	 * @param ModelFieldDateTimeInterface $dateTimeField
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitDateTimeField(ModelFieldDateTimeInterface $dateTimeField);
	
	/**
	 * Visits a float field.
	 * 
	 * @param ModelFieldFloatInterface $floatField
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFloatField(ModelFieldFloatInterface $floatField);
	
	/**
	 * Visits a date and/or time field.
	 * 
	 * @param ModelFieldIntegerInterface $integerField
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitIntegerField(ModelFieldIntegerInterface $integerField);
	
	/**
	 * Visits a json field.
	 * 
	 * @param ModelFieldJsonInterface $jsonField
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitJsonField(ModelFieldJsonInterface $jsonField);
	
	/**
	 * Visits a spatial field.
	 * 
	 * @param ModelFieldSpatialInterface $spatialField
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitSpatialField(ModelFieldSpatialInterface $spatialField);
	
	/**
	 * Visits a string field.
	 * 
	 * @param ModelFieldStringInterface $stringField
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitStringField(ModelFieldStringInterface $stringField);
	
}
