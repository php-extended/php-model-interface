<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

/**
 * ModelSchemaInterface interface file.
 * 
 * This interface represents an schema to modelize an ensemble of objects.
 * This model object is populated from a schema from a relational database.
 * 
 * @author Anastaszor
 */
interface ModelSchemaInterface extends ModelObjectProviderInterface
{
	
	/**
	 * Gets whether this schema is enabled.
	 * 
	 * @return boolean
	 */
	public function isEnabled() : bool;
	
	/**
	 * Gets whether this schema is transparent, meaning it will not appear
	 * in the name of the tables under it.
	 * 
	 * @return boolean
	 */
	public function isTransparent() : bool;
	
	/**
	 * Gets the name of the schema in the RDBMS.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Merges this schema with the other schema into a new schema object.
	 * 
	 * @param ModelSchemaInterface $schema
	 * @return ModelSchemaInterface
	 */
	public function mergeWith(ModelSchemaInterface $schema) : ModelSchemaInterface;
	
}
