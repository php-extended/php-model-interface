<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Model;

use PhpExtended\Internationalizable\InternationalizableStatusInterface;
use PhpExtended\Multiplicity\MultiplicityInterface;
use PhpExtended\Optionality\OptionalityInterface;
use Stringable;

/**
 * ModelRelationInterface interface file.
 * 
 * This interface represents a relation between two model objects. This relation,
 * depending on its multiplicity, can be translated as a foreign key on one or
 * the other table, or as a self-contained table with its primary key as both
 * model object foreign keys.
 * 
 * @author Anastaszor
 * @todo reify FieldList
 */
interface ModelRelationInterface extends Stringable
{
	
	/**
	 * Gets the name of the relation.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the comment on this relation.
	 * 
	 * @return null|string
	 */
	public function getComment();
	
	/**
	 * Gets the optionality of this field.
	 * 
	 * The optionality of this interface describes whether this field allows
	 * null or undefined, or empty values (for certain values of empty).
	 * 
	 * @return OptionalityInterface
	 */
	public function getOptionality() : OptionalityInterface;
	
	/**
	 * Gets the source model object, i.e. the point from which the relation
	 * starts.
	 * 
	 * @return ModelObjectInterface
	 */
	public function getSource() : ModelObjectInterface;
	
	/**
	 * Gets the target model object, i.e. the point from which the relation
	 * ends.
	 * 
	 * @return ModelObjectInterface
	 */
	public function getTarget() : ModelObjectInterface;
	
	/**
	 * Gets the multiplicity of this relation at the source end.
	 * 
	 * @return MultiplicityInterface
	 */
	public function getSourceMultiplicity() : MultiplicityInterface;
	
	/**
	 * Gets the multiplicity of this relation at the target end.
	 * 
	 * @return MultiplicityInterface
	 */
	public function getTargetMultiplicity() : MultiplicityInterface;
	
	/**
	 * Gets whether this relation should rely on the source model object, or
	 * its internationalized variant, assuming the choice is available, on the
	 * source end.
	 * 
	 * @return InternationalizableStatusInterface
	 */
	public function getSourceInternationalizable() : InternationalizableStatusInterface;
	
	/**
	 * Gets whether this relation should rely on the target model object, or
	 * its internationalized variant, assuming the choice is available, on the
	 * target end.
	 * 
	 * @return InternationalizableStatusInterface
	 */
	public function getTargetInternationalizable() : InternationalizableStatusInterface;
	
	/**
	 * Gets the additional fields this relation may carry. Depending on the 
	 * multiplicities on both ends of this relations, this field can be included
	 * on any of the source or the target, or can be included on the self
	 * contained table created from the relation.
	 * 
	 * @return array<integer, ModelFieldInterface>
	 */
	public function getFields() : array;
	
	/**
	 * Reverses this relation by flipping its source and its target.
	 *
	 * @return ModelRelationInterface
	 */
	public function reverse() : ModelRelationInterface;
	
	/**
	 * Merges this relation with another given data relation into a new
	 * relation object.
	 * 
	 * @param ModelRelationInterface $relation
	 * @return ModelRelationInterface
	 */
	public function mergeWith(ModelRelationInterface $relation) : ModelRelationInterface;
	
	/**
	 * Visits this relation field with the given visitor.
	 * 
	 * @param ModelRelationVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visit(ModelRelationVisitorInterface $visitor);
	
}
